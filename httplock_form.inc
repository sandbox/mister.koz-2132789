<?php

/**
 * @file
 * Contains the configuration form methods for the Httplock module.
 */

include_once "httplock.methods.inc";

/**
 * System settings for httplock.
 */
function httplock_settings_form($form, &$form_state) {
  // Basic or general settings.
  $form['basic_controls'] = array(
    '#type' => 'fieldset',
    '#title' => t('Realm Name and roles'),
  );

  // Enable/Disable all checks.
  $form['basic_controls']['httplock_enable_processing'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable httplock processing.'),
    '#default_value' => variable_get('httplock_enable_processing', httplock_default_httplock_enable_processing()),
    '#description' => t('Enable the processing of the Httplock rules, this is turned on by default.'),
  );


  // Realm name.
  $form['basic_controls']['httplock_realm_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Realm Name'),
    '#default_value' => variable_get('httplock_realm_name', httplock_default_httplock_realm_name()),
    '#required' => TRUE,
    '#description' => t('Enter the realm name that users who are attempting to view the site will see on the authentication form.'),
  );

  // Roles options - first get the roles.
  $roles = user_roles();
  $selected_roles = variable_get('httplock_selected_roles', httplock_default_httplock_selected_roles());

  // Roles options - enforce the inclusion of administrators.
  if (!in_array("3", $selected_roles)) {
    $selected_roles[] = "3";
  }

  // Roles options.
  $form['basic_controls']['httplock_selected_roles'] = array(
    '#type' => 'select',
    '#title' => t('Allowed user roles'),
    '#options' => $roles,
    '#multiple' => TRUE,
    '#required' => FALSE,
    '#description' => t("Specify which role's are allowed to log in (users with the admin role are always allowed)."),
    '#default_value' => $selected_roles,
  );

  // Enable/disable IP controls.
  $form['basic_controls']['httplock_enable_ip_options'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable IP options'),
    '#default_value' => variable_get('httplock_enable_ip_options', httplock_default_httplock_enable_ip_options()),
  );

  // IP restrictions.
  $form['ip_restrictions'] = array(
    '#type' => 'fieldset',
    '#title' => t('Whitelist and auto_auth'),
    '#states' => array(
      'visible' => array(
        ':input[name="httplock_enable_ip_options"]' => array('checked' => TRUE),
      ),
    ),
  );

  // Enable Whitelist.
  $form['ip_restrictions']['httplock_whitelist_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable Whitelist'),
    '#default_value' => variable_get('httplock_whitelist_enabled', httplock_default_httplock_whitelist_enabled()),
    '#description' => t("By default, all client browsers are allowed, enabling the whitelist means that an administrator can stop all but specified viewers by ip's."),
  );

  // White-listed IP's.
  $form['ip_restrictions']['httplock_whitelist_ips'] = array(
    '#type' => 'textarea',
    '#title' => t("White list Ip's"),
    '#default_value' => variable_get('httplock_whitelist_ips', httplock_default_httplock_whitelist_ips()),
    '#description' => t('Computers and networks viewing this site from ip addresses other than listed here will be automatically denied, put a star ot leave this blank to allow all clients.'),
    '#states' => array(
      'visible' => array(
        ':input[name="httplock_whitelist_enabled"]' => array('checked' => TRUE),
      ),
    ),
    '#element_validate' => array('httplock_ip_string_validate'),
  );

  // Auto-auth IP's.
  $form['ip_restrictions']['httplock_auto_auth_ips'] = array(
    '#type' => 'textarea',
    '#title' => t("Auto-auth Ip's"),
    '#default_value' => variable_get('httplock_auto_auth_ips', httplock_default_httplock_auto_auth_ips()),
    '#description' => t("Computers and networks viewing this site from the ip's entered here will not be asked for a password, multiple results split by a comma. note: user details or roles will not be tested on sessions origonating from these IP's."),
    '#element_validate' => array('httplock_ip_string_validate'),
  );

  // Denied response.
  $form['denied_response'] = array(
    '#type' => 'fieldset',
    '#title' => t('Authentication denied response type'),
  );

  // Response type (node or raw html/text).
  $denied_response_sources = array(
    HTTPLOCK_DENIED_TYPE_TEXT => 'raw text/html',
    HTTPLOCK_DENIED_TYPE_NODE => 'use a node',
  );
  $form['denied_response']['httplock_denied_response_type'] = array(
    '#type' => 'select',
    '#title' => t('Denied response'),
    '#options' => $denied_response_sources,
    '#multiple' => FALSE,
    '#description' => t('Please select whether you would like plain html or a node to be displayed if the authentication fails.'),
    '#default_value' => variable_get('httplock_denied_response_type', httplock_default_httplock_denied_response_type()),
  );

  // Denied Text/HTML response.
  $form['denied_response']['httplock_denied_html'] = array(
    '#type' => 'textarea',
    '#title' => t('Denied message: HTML'),
    '#default_value' => variable_get('httplock_denied_html', httplock_default_httplock_denied_html()),
    '#description' => t('If a user clicks "cancel" at the login prompt, they will be greeted with text/html in this field.'),
    '#states' => array(
      'visible' => array(
        ':input[name="httplock_denied_response_type"]' => array('value' => HTTPLOCK_DENIED_TYPE_TEXT),
      ),
    ),
  );


  $form['denied_response']['httplock_denied_node'] = array(
    '#type' => 'select',
    '#title' => t('Denied message: node'),
    '#options' => httplock_get_node_list(),
    '#multiple' => FALSE,
    '#description' => t('Please select the node/page you would like displayed if the authentication fails.'),
    '#default_value' => variable_get('httplock_denied_node', httplock_default_httplock_denied_node()),
    '#states' => array(
      'visible' => array(
        ':input[name="httplock_denied_response_type"]' => array('value' => HTTPLOCK_DENIED_TYPE_NODE),
      ),
    ),
  );

  return (system_settings_form($form));
}

/**
 * Parses strings of IP addresses separated by comma's.
 */
function httplock_ip_string_validate($element, &$form_state, $form) {
  $ip_enabled = $form['basic_controls']['httplock_enable_ip_options']['#value'] == 1;

  if (!$ip_enabled) {
    return;
  }

  if (empty($element['#value'])) {
    return;
  }
  else {
    $items = explode(',', $element['#value']);
    foreach ($items as $item) {
      if (!httplock_parse_ip(trim($item))) {
        form_error($element, t('Invalid IP address entered for @title (@item)', array('@title' => $element['#title'], '@item' => $item)));
      }
    }
  }
}
