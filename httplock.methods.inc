<?php

/**
 * @file
 * Contains the helper functions for the Httplock module.
 */

/**
 * Some definitions for logic.
 */
define("HTTPLOCK_DENIED_TYPE_TEXT", 1);
define("HTTPLOCK_DENIED_TYPE_NODE", 2);


/**
 * Implemented in uninstall - removes all of the variables.
 */
function httplock_delete_all_variables() {
  foreach (httplock_variables_list() as $value) {
    if (is_array($value)) {
      variable_del($value[0]);
    }
  }
}

/**
 * Implemented in install - sets up all of the variables.
 */
function httplock_write_all_variables() {
  foreach (httplock_variables_list() as $value) {
    if (is_array($value)) {
      variable_set($value[0], $value[1]);
    }
  }
}

/**
 * Configuration: variable defaults and list.
 */
function httplock_variables_list() {
  $settings_variables = array();
  $settings_variables[] = array('httplock_realm_name', 'Development Area');
  $settings_variables[] = array('httplock_selected_roles', array(
    1 => 1,
    2 => 2,
    3 => 3,
    ),
  );
  $settings_variables[] = array('httplock_enable_processing', TRUE);
  $settings_variables[] = array('httplock_enable_ip_options', FALSE);
  $settings_variables[] = array('httplock_whitelist_enabled', FALSE);
  $settings_variables[] = array('httplock_whitelist_ips', '127.0.0.1');
  $settings_variables[] = array('httplock_auto_auth_ips', '');
  $settings_variables[] = array('httplock_denied_response_type', HTTPLOCK_DENIED_TYPE_TEXT);
  $settings_variables[] = array('httplock_denied_html', 'login required');
  $settings_variables[] = array('httplock_denied_node', 1);

  return $settings_variables;
}

/**
 * Get the default configuration settings per setting.
 */
function httplock_variables_default($variable_name) {
  $list = httplock_variables_list();
  foreach ($list as $value) {
    if ($value[0] == $variable_name) {
      return $value[1];
    }
  }
}

/**
 * Config helper method: gets the default setting for httplock_realm_name.
 */
function httplock_default_httplock_realm_name() {
  return httplock_variables_default('httplock_realm_name');
}

/**
 * Config helper method: gets the default setting for enable_ip_options.
 */
function httplock_default_httplock_enable_ip_options() {
  return httplock_variables_default('httplock_enable_ip_options');
}

/**
 * Config helper method: gets the default setting for enable_ip_options.
 */
function httplock_default_httplock_enable_processing() {
  return httplock_variables_default('httplock_enable_processing');
}

/**
 * Config helper method: gets the default setting for selected_roles.
 */
function httplock_default_httplock_selected_roles() {
  return httplock_variables_default('httplock_selected_roles');
}

/**
 * Config helper method: gets the default setting for whitelist_enabled.
 */
function httplock_default_httplock_whitelist_enabled() {
  return httplock_variables_default('httplock_whitelist_enabled');
}

/**
 * Config helper method: gets the default setting for whitelist_ips.
 */
function httplock_default_httplock_whitelist_ips() {
  return httplock_variables_default('httplock_whitelist_ips');
}

/**
 * Config helper method: gets the default setting for auto_auth_ips.
 */
function httplock_default_httplock_auto_auth_ips() {
  return httplock_variables_default('httplock_auto_auth_ips');
}

/**
 * Config helper method: gets the default setting for denied_response_type.
 */
function httplock_default_httplock_denied_response_type() {
  return httplock_variables_default('httplock_denied_response_type');
}

/**
 * Config helper method: gets the default setting for denied_html.
 */
function httplock_default_httplock_denied_html() {
  return httplock_variables_default('httplock_denied_html');
}

/**
 * Config helper method: gets the default setting for denied_node.
 */
function httplock_default_httplock_denied_node() {
  return httplock_variables_default('httplock_denied_node');
}

/**
 * Configuration helper method: whitelist enable.
 */
function httplock_enable_processing($input = "") {
  variable_set("httplock_enable_processing", TRUE);
}

/**
 * Configuration helper method: whitelist disable.
 */
function httplock_disable_processing($input = "") {
  variable_set("httplock_enable_processing", FALSE);
}

/**
 * Configuration helper method: whitelist enable.
 */
function httplock_enable_whitelist($input = "") {
  variable_set("httplock_whitelist_enabled", TRUE);
}

/**
 * Configuration helper method: whitelist disable.
 */
function httplock_disable_whitelist($input = "") {
  variable_set("httplock_whitelist_enabled", FALSE);
}

/**
 * Configuration helper method: whitelist add.
 */
function httplock_add_whitelist($input = "") {
  $whitelist = explode(',', variable_get('httplock_whitelist_ips'));
  $whitelist[] = $input;
  variable_set("httplock_whitelist_ips", implode(", ", httplock_cleanse_array($whitelist)));
}

/**
 * Configuration helper method: whitelist remove.
 */
function httplock_remove_whitelist($input = "") {
  $whitelist = explode(',', variable_get('httplock_whitelist_ips'));
  foreach ($whitelist as $key => $value) {
    if (trim($value) == $input) {
      unset($whitelist[$key]);
    }
    variable_set("httplock_whitelist_ips", implode(", ", httplock_cleanse_array($whitelist)));
  }
}

/**
 * Configuration helper method: auto_auth add.
 */
function httplock_add_auto_auth($input = "") {
  $auto_auth_list = explode(',', variable_get('httplock_auto_auth_ips'));
  $auto_auth_list[] = $input;
  variable_set("httplock_auto_auth_ips", implode(", ", httplock_cleanse_array($auto_auth_list)));
}

/**
 * Configuration helper method: auto_auth remove.
 */
function httplock_remove_auto_auth($input = "") {
  $auto_auth_list = explode(',', variable_get('httplock_auto_auth_ips'));
  foreach ($auto_auth_list as $key => $value) {
    if (trim($value) == $input) {
      unset($auto_auth_list[$key]);
    }
  }
  variable_set("httplock_auto_auth_ips", implode(", ", httplock_cleanse_array($auto_auth_list)));
}

/**
 * Tooling method: Clean array, trims and comma's things nicely.
 */
function httplock_cleanse_array($input_array) {
  $return_array = array();
  foreach ($input_array as $value) {
    if (!empty($value) && !in_array(trim($value), $return_array)) {
      $return_array[] = trim($value);
    }
  }
  return $return_array;
}

/**
 * Tooling method: Rudimentry ip parsing test.
 */
function httplock_parse_ip($ip_address) {
  if (preg_match("/^(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})$/", $ip_address)) {
    $parts = explode(".", $ip_address);
    foreach ($parts as $part) {
      if (intval($part) > 255 || intval($part) < 0) {
        return FALSE;
      }
    }
    return TRUE;
  }
  return FALSE;
}

/**
 * Tooling method: get a list of nodes
 */

function httplock_get_node_list() {
  $denied_node_ids = db_select('node', 'n')
      ->fields('n', array('nid'))
      ->fields('n', array('title'))
      ->fields('n', array('type'))
      ->execute()
      ->fetchCol();
  $denied_nodes_possible_options = node_load_multiple($denied_node_ids);
  $denied_node_options = array();
  foreach ($denied_nodes_possible_options as $nodeid => $node) {
    $denied_node_options[$nodeid] = $node->title;
  }

  return $denied_node_options;
}

/**
 * Tooling method: user-auth passed
 */

function httplock_is_auth_passed() {
    // Member Variables.
  $entered_user = "";
  $entered_password = "";

  // Split user/pass parts - this is because some browsers and php versions do
  // not interact the same with the 400 auth array.
  if (array_key_exists('HTTP_AUTHORIZATION', $_SERVER)) {
    list($_SERVER['PHP_AUTH_USER'], $_SERVER['PHP_AUTH_PW']) = explode(':', base64_decode(substr($_SERVER['HTTP_AUTHORIZATION'], 6)));
    $entered_user = $_SERVER['PHP_AUTH_USER'];
    $entered_password = $_SERVER['PHP_AUTH_PW'];
  }
  elseif (array_key_exists('PHP_AUTH_USER', $_SERVER) && array_key_exists('PHP_AUTH_PW', $_SERVER)) {
    $entered_user = $_SERVER['PHP_AUTH_USER'];
    $entered_password = $_SERVER['PHP_AUTH_PW'];
  }

  $user_auth_passed = (isset($_SESSION) && array_key_exists('httplock_auth_passed', $_SESSION) && $_SESSION['httplock_auth_passed'] == TRUE && !empty($entered_user) && !empty($entered_password));

  // If the user is logged in then stop rules.
  global $user;
  if ($user->uid || $user_auth_passed) {
    return TRUE;
  }

  return FALSE;
}
