This module forces people to authentication their visit with an http password
much like using an htpasswd file the difference being that the user is
authenticated against the drupal users table but the user is not logged in to 
the drupal site.

Where this becomes useful is on a staging site where people need to see and test
the site but the site shouldn't be live to the open internet or search engines.

--------------------------------------------------------------------------------
Dependencies:
--------------------------------------------------------------------------------

- None

--------------------------------------------------------------------------------
Installation:
--------------------------------------------------------------------------------

Download the module and simply copy it into your contributed modules folder:
[for example, your_drupal_path/sites/all/modules] and enable it from the
modules administration/management page.
More information at: Installing contributed modules (Drupal 7).
OR you could use drush:
* drush dl httplock
* drush en httplock

Please note: when this module is enabled or the configuration is changed, the
site will be blocking based on configuration on the next page refresh, use drush
to disable the module, configure it or reset the configuration to defaults.

--------------------------------------------------------------------------------
Configuration
--------------------------------------------------------------------------------

Realm Name:
You can configure the module to display what ever realm title you wish.
Default: "Development Area"

Selected Roles:
Access to the site can be restricted by user role, the administrator role is
always approved.
Default: anonymous, authenticated and administrator.

Enable IP options:
If desired, HttpLock can be configured to automatically authenticate some IPs or
deny all logins (even with the correct credentials) from all but specified IPs.
To enable these options the "Enable IP options" checkbox must be checked.
Default: unchecked

Enable White List:
The white list can be disabled, this is usually the desired practice because the
website is already protected by only allowing authenticated users access to the
site. To enable the whitelist, check this box.
Default: unchecked

White List:
Clients viewing from a computer not in the white list will always be denied. The
White list only supports IP addresses.
Default: blank

Auto-auth list:
You can enter a comma separated list of IP addresses that immediately pass the
authentication without entering any information, provided they are in the white
list. The auto-auth only accepts IP Addresses.
Default: "127.0.0.1"

Denied response type:
When a user enters incorrect details, they will be greeted with an error page.
This page can be raw text/html or it can be a node, select the correct option
in this box.
Default: "raw text/html"

Denied message HTML:
If the denied response type is set to "raw text/html" failed authentication
attempts will be met by the text in this box.
Default: "Development Area"

Denied message Node:
If the denied response type is set to "use a node" failed authentication
attempts will be met by the page specified.
Default: node 1

--------------------------------------------------------------------------------
Drush Commands
--------------------------------------------------------------------------------

Command: Reset Config
Description: Resets all of the configuration to defaults.
ShortCode: htlreset
Example:
  'drush htlreset'
  Resets all of the configuration to defaults.

Command: Wnable Whitelist
Description: Enables the whitelist processing.
ShortCode: htlenw
Example:
  'drush htlensw'
  Enables the whitelist processing, this means client computers not
  origonating from an IP address in the white list will be denied.

Command: Disable Whitelist
Description: Disables the whitelist processing.
ShortCode: htldisw
Example:
  'drush htldisw'
  Disables the whitelist processing, this means that any computer with the
  right credentials will be able to access the site.

Command: Add Whitelist
Description: Adds an ip address onto the whitelist.
ShortCode: htladdw
Example:
  'drush htladdw 127.0.0.1'
  Adds the 127.0.0.1 record to the white list.

Command: Remove Whitelist
Description: Removes an ip address from the whitelist.
ShortCode: htlrmw
Example:
  'drush htlrmw 127.0.0.1'
  Removes the 127.0.0.1 record to the white list.

Command: Add Auto Auth
Description: Adds an ip address onto the auto-auth list.
ShortCode: htladda
Example:
  'drush htladdw 127.0.0.1'
  Adds the 127.0.0.1 record to the auto-auth list.

Command: Remove Auto Auth
Description: Removes the specified IP from the auto-auth list.
ShortCode: htlrma
Example:
  'drush htlrma 127.0.0.1'
  Removes the 127.0.0.1 record from the auto-auth list.


--------------------------------------------------------------------------------
Additional information
--------------------------------------------------------------------------------

This module is safe with drush, it will detect that the user is not the drush
cli and silently skip authentication.

Uninstalling the module removes all configuration options.

--------------------------------------------------------------------------------
Possible Future additions
--------------------------------------------------------------------------------

* Creating a role permission for this purpose specifically
* Create the facility to lock down only specified areas of the site by taxonomy
* Create the facility to lock down only specified areas of the site by url path
* integrate drupal login flood protection to protect brute force attacks
