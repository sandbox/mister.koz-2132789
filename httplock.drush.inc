<?php

/**
 * @file
 * Drush methods, aliases and descriptions for the Httplock module.
 */

include_once 'httplock.methods.inc';

/**
 * Httplock drush commands.
 */
function httplock_drush_command() {
  $items = array();

  $items['reset_config'] = array(
    'description' => 'Resets all of the configuration to defaults.',
    'arguments' => array(),
    'examples' => array('drush htlreset' => 'Resets all of the configuration to defaults.',),
    'aliases' => array('htlreset'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_DATABASE,
  );

  $items['enable_processing'] = array(
    'description' => 'Enables the processing of all Httplock rules.',
    'arguments' => array(),
    'examples' => array('drush htlenp' => 'Enables the processing of all Httplock rules.',),
    'aliases' => array('htlenp'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_DATABASE,
  );

  $items['disable_processing'] = array(
    'description' => 'Disables the processing of all Httplock rules.',
    'arguments' => array(),
    'examples' => array('drush htldisp' => 'Disables the processing of all Httplock rules.',),
    'aliases' => array('htldisp'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_DATABASE,
  );

  $items['realm_name'] = array(
    'description' => 'Changes the Realm Name of the http 401 message.',
    'arguments' => array('text' => 'The text that you wish the realm name to be.',),
    'examples' => array('drush htlrn "My special Realm"' => 'CHanges the Realm Name to "My special Realm".',),
    'aliases' => array('htlrn'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_DATABASE,
  );

  $items['add_approved_role'] = array(
    'description' => 'Adds a user role to the approved roles list. ',
    'arguments' => array('id' => 'The id of the user role to add to the approved roles .',),
    'examples' => array('drush htladdr 1' => 'Adds the role with the ID of 1 to the approved roles list.',),
    'aliases' => array('htladdr'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_DATABASE,
  );

  $items['remove_approved_role'] = array(
    'description' => 'Removes a user role from the approved roles list. ',
    'arguments' => array('id' => 'The id of the user role to remove from the approved roles .',),
    'examples' => array('drush htlrmr 1' => 'Removes the role with the ID of 1 from the approved roles list.',),
    'aliases' => array('htlrmr'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_DATABASE,
  );

  $items['enable_ip_options'] = array(
    'description' => 'Enables the IP restriction and authentication options.',
    'arguments' => array(),
    'examples' => array('drush htlenip' => 'Enables the IP restriction and authentication options.',),
    'aliases' => array('htlenip'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_DATABASE,
  );

  $items['disable_ip_options'] = array(
    'description' => 'Disables the IP restriction and authentication options.',
    'arguments' => array(),
    'examples' => array('drush htldisip' => 'Disables the IP restriction and authentication options.',),
    'aliases' => array('htldisip'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_DATABASE,
  );

  $items['set_response_type_text'] = array(
    'description' => 'Sets the denied response type to plain text/HTML.',
    'arguments' => array(),
    'examples' => array('drush htlrestxt' => 'Sets the denied response type to plain text/HTML.',),
    'aliases' => array('htlrestxt'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_DATABASE,
  );

  $items['set_response_type_node'] = array(
    'description' => 'Sets the denied response type to show Drupal Page/Node.',
    'arguments' => array(),
    'examples' => array('drush htlresnode' => 'Sets the denied response type to show Drupal Page/Node.',),
    'aliases' => array('htlresnode'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_DATABASE,
  );

  $items['set_response_text'] = array(
    'description' => 'Sets the response text for the plain text/HTML error message.',
    'arguments' => array('text' => 'The text to be displayed on error.',),
    'examples' => array('drush htlsettext "Access Denied"' => 'Sets the response text for the plain text/HTML error message to "Access Denied"',),
    'aliases' => array('htlsettext'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_DATABASE,
  );

  $items['set_response_node'] = array(
    'description' => 'Sets the node to be displayed on error.',
    'arguments' => array('id' => 'The node to be displayed on error.',),
    'examples' => array('drush htlsetnode 1' => 'Sets the node that will be displayed on error to 1.',),
    'aliases' => array('htlsetnode'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_DATABASE,
  );

  $items['enable_whitelist'] = array(
    'description' => 'Enables the whitelist processing.',
    'arguments' => array(),
    'examples' => array('drush htlenw' => 'Enables the whitelist processing, this means client computers not origonating from an IP address in the white list will be denied.',),
    'aliases' => array('htlenw'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_DATABASE,
  );

  $items['disable_whitelist'] = array(
    'description' => 'Disables the whitelist processing.',
    'arguments' => array(),
    'examples' => array('drush htldisw' => 'Disables the whitelist processing, this means that any computer with the right credentials will be able to access the site.',),
    'aliases' => array('htldisw'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_DATABASE,
  );

  $items['add_whitelist'] = array(
    'description' => 'Adds an ip address onto the whitelist.',
    'arguments' => array('ip' => 'The ip address to add to the whitelist.',),
    'examples' => array('drush htladdw 127.0.0.1' => 'Adds the 127.0.0.1 record to the white list.',),
    'aliases' => array('htladdw'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_DATABASE,
  );

  $items['remove_whitelist'] = array(
    'description' => 'Removes an ip address from the whitelist.',
    'arguments' => array('ip' => 'The ip address to remove from the whitelist.',),
    'examples' => array('drush htlrmw 127.0.0.1' => 'Removes the 127.0.0.1 record to the white list.',),
    'aliases' => array('htlrmw'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_DATABASE,
  );

  $items['add_auto_auth'] = array(
    'description' => 'Adds an ip address onto the auto-auth list.',
    'arguments' => array('ip' => 'The ip address to add to the auto-auth list.',),
    'examples' => array('drush htladdw 127.0.0.1' => 'Adds the 127.0.0.1 record to the auto-auth list.',),
    'aliases' => array('htladda'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_DATABASE,
  );

  $items['remove_auto_auth'] = array(
    'description' => 'Removes the specified IP from the auto-auth list.',
    'arguments' => array('ip' => 'The ip address to remove from the auto-auth list.',),
    'examples' => array('drush htlrma 127.0.0.1' => 'Removes the 127.0.0.1 record from the auto-auth list.',),
    'aliases' => array('htlrma'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_DATABASE,
  );

  return $items;
}

/**
 * Reset the configuration to default.
 */
function drush_httplock_reset_config() {
  httplock_delete_all_variables();
  httplock_write_all_variables();
}

/**
 * Validate enable Httplock rules processing.
 */
function drush_httplock_enable_processing_validate() {
  $processing_enabled = variable_get('httplock_enable_processing', httplock_default_httplock_enable_processing());

  if ($processing_enabled) {
    return drush_set_error('ALREADY_ENABLED', dt('The rules processing is already enabled.'));
  }
}

/**
 * Enable Httplock rules processing.
 */
function drush_httplock_enable_processing() {
  variable_set('httplock_enable_processing', TRUE);
}

/**
 * Validate disable Httplock rules processing.
 */
function drush_httplock_disable_processing_validate() {
  $processing_enabled = variable_get('httplock_enable_processing', httplock_default_httplock_enable_processing());

  if (!$processing_enabled) {
    return drush_set_error('ALREADY_DISABLED', dt('The rules processing is already disabled.'));
  }
}

/**
 * Disable Httplock rules processing.
 */
function drush_httplock_disable_processing() {
  variable_set('httplock_enable_processing', FALSE);
}

/**
 * Validate Realm Name.
 */
function drush_httplock_realm_name_validate($text = '') {
  $text = trim($text);
  if (empty($text)) {
    return drush_set_error('INVALID_TEXT', dt('Text must not be empty.'));
  }
}

/**
 * Set Realm Name.
 */
function drush_httplock_realm_name($text = '') {
  $text = trim($text);
  variable_set('httplock_realm_name', $text);
}

/**
 * Validate remove approved role.
 */
function drush_httplock_add_approved_role_validate($id = '') {
  $id = intval(trim($id));

  if (empty($id)) {
    return drush_set_error('EMPTY_ID', dt('Error parsing ID, it appears to be empty.'));
  }

  // Check if the supplied ID is the administrator role.
  if ($id == 3) {
    return drush_set_error('ADMIN_ID', dt('Administrator is always enabled.'));
  }

  // Check if the supplied ID in the roles list.
  $system_roles = user_roles();

  if (!in_array($id, array_keys($system_roles))) {
    return drush_set_error('INVALID_ID', dt('Error parsing ID, the supplied ID is not in the system roles list.'));
  }

  // Check if the supplied ID is already in the roles list.
  $httplock_roles = variable_get('httplock_selected_roles', httplock_default_httplock_selected_roles());

  if (in_array($id, $httplock_roles)) {
    return drush_set_error('ALREADY_APPROVED', dt('The specified ID is already in the roles list.'));
  }
}

/**
 * Add approved role.
 */
function drush_httplock_add_approved_role($id = '') {
  $id = intval(trim($id));
  $httplock_roles = variable_get('httplock_selected_roles', httplock_default_httplock_selected_roles());

  // Enforce the inclusion of administrators.
  if (!in_array(3, $httplock_roles)) {
    $httplock_roles[] = 3;
  }

  // Finally add the id.
  if (!in_array($id, $httplock_roles)) {
    $httplock_roles[$id] = $id;
  }
  sort($httplock_roles);

  variable_set('httplock_selected_roles', $httplock_roles);
}

/**
 * Validate remove approved role.
 */
function drush_httplock_remove_approved_role_validate($id = '') {
  $id = intval(trim($id));

  if (empty($id)) {
    return drush_set_error('EMPTY_ID', dt('Error parsing ID, it appears to be empty.'));
  }

  // Check if the supplied ID is the administrator role.
  if ($id == 3) {
    return drush_set_error('ADMIN_ID', dt('Administrator is always enabled.'));
  }

  // Get the roles list.
  $httplock_roles = variable_get('httplock_selected_roles', httplock_default_httplock_selected_roles());

  if (!in_array($id, $httplock_roles)) {
    return drush_set_error('NOT_FOUND', dt('The specified ID is not in the roles list.'));
  }
}

/**
 * Remove approved role.
 */
function drush_httplock_remove_approved_role($id = '') {
  $id = intval(trim($id));

  // Get the roles list.
  $httplock_roles = variable_get('httplock_selected_roles', httplock_default_httplock_selected_roles());

  foreach ($httplock_roles as $key => $value) {
    if ($value == $id) {
      unset($httplock_roles[$key]);
      variable_set('httplock_selected_roles', $httplock_roles);
      return;
    }
  }
}

/**
 * Validate enable IP options.
 */
function drush_httplock_enable_ip_options_validate() {
  $options_enabled = variable_get('httplock_enable_ip_options', httplock_default_httplock_enable_ip_options());

  if ($options_enabled) {
    return drush_set_error('ALREADY_ENABLED', dt('The ip options are already enabled.'));
  }
}

/**
 * Enable IP options.
 */
function drush_httplock_enable_ip_options() {
  variable_set('httplock_enable_ip_options', TRUE);
}

/**
 * Validate disable IP options.
 */
function drush_httplock_disable_ip_options_validate() {
  $options_enabled = variable_get('httplock_enable_ip_options', httplock_default_httplock_enable_ip_options());

  if (!$options_enabled) {
    return drush_set_error('ALREADY_DISABLED', dt('The ip options are already disabled.'));
  }
}

/**
 * Disable IP options.
 */
function drush_httplock_disable_ip_options() {
  variable_set('httplock_enable_ip_options', FALSE);
}

/**
 * Validate set denied response type to text.
 */
function drush_httplock_set_response_type_text_validate() {
  $response_type = variable_get('httplock_denied_response_type', httplock_default_httplock_denied_response_type());

  if ($response_type == HTTPLOCK_DENIED_TYPE_TEXT) {
    return drush_set_error('ALREADY_SET', dt('The response type is already set to raw text/html.'));
  }
}

/**
 * Set denied response type to text.
 */
function drush_httplock_set_response_type_text() {
  variable_set('httplock_denied_response_type', HTTPLOCK_DENIED_TYPE_TEXT);
}

/**
 * Validate set denied response type to node.
 */
function drush_httplock_set_response_type_node_validate() {
  $response_type = variable_get('httplock_denied_response_type', httplock_default_httplock_denied_response_type());

  if ($response_type == HTTPLOCK_DENIED_TYPE_NODE) {
    return drush_set_error('ALREADY_SET', dt('The response type is already set to display a node.'));
  }
}

/**
 * Set denied response type to node.
 */
function drush_httplock_set_response_type_node() {
  variable_set('httplock_denied_response_type', HTTPLOCK_DENIED_TYPE_NODE);
}


/**
 * Validate set denied response text.
 */
function drush_httplock_set_response_text_validate($text = '') {
  $text = trim($text);

  // Test if empty.
  if (empty($text)) {
    return drush_set_error('EMPTY_TEXT', dt('Error parsing text, cannot be empty.'));
  }
}

/**
 * Set denied response text.
 */
function drush_httplock_set_response_text($text = '') {
  variable_set('httplock_denied_html', $text);
}

/**
 * Validate set denied response node.
 */
function drush_httplock_set_response_node_validate($id = '') {
  $id = intval(trim($id));

  // Test if empty.
  if (empty($id)) {
    return drush_set_error('EMPTY_ID', dt('Error parsing node id, cannot be empty.'));
  }

  // Test if the node exists.
  $possible_node_query = db_select('node', 'n')
      ->fields('n', array('nid'))
      ->fields('n', array('title'))
      ->fields('n', array('type'))
      ->execute()
      ->fetchCol();
  $possible_nodes = node_load_multiple($possible_node_query);

  if (!array_key_exists($id, $possible_nodes)) {
    return drush_set_error('NOT_FOUND', dt('Error parsing node id, node not found.'));
  }
}

/**
 * Set denied response node.
 */
function drush_httplock_set_response_node($id = '') {
  $id = intval(trim($id));
  variable_set('httplock_denied_node', $id);
}

/**
 * Validate disable whitelist.
 */
function drush_httplock_disable_whitelist_validate() {
  if (variable_get("httplock_whitelist_enabled") == FALSE) {
    return drush_set_error('NOT_ENABLED', dt('Error, white list is already disabled.'));
  }
}

/**
 * Disable whitelist.
 */
function drush_httplock_disable_whitelist() {
  httplock_disable_whitelist();
}

/**
 * Validate enable whitelist.
 */
function drush_httplock_enable_whitelist_validate() {
  if (variable_get("httplock_whitelist_enabled") == TRUE) {
    return drush_set_error('ALREADY_ENABLED', dt('Error, white list is already enabled.'));
  }
}

/**
 * Enable whitelist.
 */
function drush_httplock_enable_whitelist() {
  httplock_enable_whitelist();
}

/**
 * Validate add whitelist IP.
 */
function drush_httplock_add_whitelist_validate($ip = '') {
  $ip = trim($ip);
  if (!httplock_parse_ip($ip)) {
    return drush_set_error('INVALID_IP', dt('Error parsing IP, please enter a valid IP address.'));
  }

  $whitelist = explode(',', variable_get('httplock_whitelist_ips'));
  foreach ($whitelist as $value) {
    if (trim($value) == $ip) {
      return drush_set_error('DUPLICATE_IP', dt('Error parsing IP, specified IP is already in the whitelist.'));
    }
  }
}

/**
 * Add whitelist IP.
 */
function drush_httplock_add_whitelist($ip = '') {
  httplock_add_whitelist($ip);
}

/**
 * Validate remove whitelist IP.
 */
function drush_httplock_remove_whitelist_validate($ip = '') {
  $ip = trim($ip);
  if (!httplock_parse_ip($ip)) {
    return drush_set_error('INVALID_IP', dt('Error parsing IP, please enter a valid IP address.'));
  }
  $whitelist = explode(',', variable_get('httplock_whitelist_ips'));
  foreach ($whitelist as $value) {
    if (trim($value) == $ip) {
      return;
    }
  }
  return drush_set_error('NOT_FOUND', dt('Error parsing IP, record does not exist?.'));
}

/**
 * Remove whitelist IP.
 */
function drush_httplock_remove_whitelist($ip = '') {
  httplock_remove_whitelist($ip);
}

/**
 * Validate add auto auth IP.
 */
function drush_httplock_add_auto_auth_validate($ip = '') {
  $ip = trim($ip);
  if (!httplock_parse_ip($ip)) {
    return drush_set_error('INVALID_IP', dt('Error parsing IP, please enter a valid IP address.'));
  }

  $auto_auth_list = explode(',', variable_get('httplock_auto_auth_ips'));
  foreach ($auto_auth_list as $value) {
    if (trim($value) == $ip) {
      return drush_set_error('DUPLICATE_IP', dt('Error parsing IP, specified IP is already in the auto-auth list.'));
    }
  }
}

/**
 * Add auto auth IP.
 */
function drush_httplock_add_auto_auth($ip = '') {
  httplock_add_auto_auth($ip);
}

/**
 * Validate remove auto auth IP.
 */
function drush_httplock_remove_auto_auth_validate($ip = '') {
  $ip = trim($ip);
  if (!httplock_parse_ip($ip)) {
    return drush_set_error('INVALID_IP', dt('Error parsing IP, please enter a valid IP address.'));
  }
  $auto_auth_list = explode(',', variable_get('httplock_auto_auth_ips'));
  foreach ($auto_auth_list as $value) {
    if (trim($value) == $ip) {
      return;
    }
  }
  return drush_set_error('NOT_FOUND', dt('Error parsing IP, record does not exist?.'));
}

/**
 * Remove auto auth IP.
 */
function drush_httplock_remove_auto_auth($ip = '') {
  httplock_remove_auto_auth($ip);
}
